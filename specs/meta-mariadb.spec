%global app                     mysql
%global user                    %{app}
%global group                   %{app}

%global d_home                  /home
%global d_storage               %{d_home}/storage_01
%global d_data                  %{d_storage}/data_02

%global d_cnf                   %{_sysconfdir}/my.cnf.d
%global d_service               %{_sysconfdir}/systemd/system/mariadb.service.d

Name:                           meta-mariadb
Version:                        1.0.1
Release:                        2%{?dist}
Summary:                        META-package for install and configure MariaDB
License:                        GPLv3

Source10:                       my.cnf
Source20:                       service.homedir.conf
Source21:                       service.limits.conf

Requires:                       meta-repos MariaDB-server MariaDB-client

%description
META-package for install and configure MariaDB.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -dp -m 0755 %{buildroot}%{d_data}/%{app}

%{__install} -Dp -m 0644 %{SOURCE10} \
  %{buildroot}%{d_cnf}/custom.my.cnf
%{__install} -Dp -m 0644 %{SOURCE20} \
  %{buildroot}%{d_service}/custom.homedir.conf
%{__install} -Dp -m 0644 %{SOURCE21} \
  %{buildroot}%{d_service}/custom.limits.conf


%files
%attr(0700,%{user},%{group}) %dir %{d_data}/%{app}
%config %{d_cnf}/custom.my.cnf
%config %{d_service}/custom.homedir.conf
%config %{d_service}/custom.limits.conf


%changelog
* Sun Jul 28 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-2
- UPD: SPEC-file.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.1-1
- UPD: MariaDB config.
- NEW: SystemD config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-15
- UPD: MariaDB config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-14
- UPD: MariaDB config.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-13
- UPD: MariaDB config.

* Sat Jul 06 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-12
- UPD: MariaDB config.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-11
- UPD: MariaDB config.

* Fri Jul 05 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-10
- UPD: SPEC-file.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-9
- UPD: DB directory.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-8
- UPD: SPEC-file.

* Mon Jul 01 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-7
- UPD: MariaDB config.

* Fri Apr 19 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-6
- UPD: Directory structure.

* Sun Apr 14 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-5
- UPD: Directory structure.

* Sat Apr 13 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-4
- NEW: 1.0.0-4.

* Wed Apr 10 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-3
- NEW: 1.0.0-3.

* Sat Mar 30 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-2
- NEW: 1.0.0-2.

* Wed Jan 02 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
